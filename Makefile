all: install run

install: 
	composer install
	npm install
	php bin/console doctrine:migrations:diff
	php bin/console doctrine:migrations:migrate

run:
	php bin/console server:start

stop:
	php bin/console server:stop

migrations:
	php bin/console doctrine:migrations:diff
	php bin/console doctrine:migrations:migrate

entity: 
	php bin/console doctrine:generate:entities

encore-watch:
	npm run watch