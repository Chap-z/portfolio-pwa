<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


class UserFixtures extends Fixture
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setFirstname('Charlotte');
        $user->setLastname('Lacroix');
        $user->setEmail('lacroix.cha@gmail.com');

        $user->setUsername('admin');

        $user->setPassword($this->encoder->encodePassword($user,'admin'));

        $user->setRole('ROLE_ADMIN');

        $manager->persist($user);
        $manager->flush();

    }
}
